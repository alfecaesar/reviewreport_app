<?php include_once('config.php');
if(isset($_REQUEST['submit']) and $_REQUEST['submit']!=""){
	extract($_REQUEST);

		
		$reportCount	=	$db->getQueryCount('reports','id');
		if($reportCount[0]['total']<9999){
			$data	=	array(
							'date'=>$date,
							'roomnumber'=>$roomnumber,
							'roomtype'=>$roomtype,
							'brand'=>$brand,
							'nationality'=>$nationality,
							'reviewscore'=>$reviewscore,
							'staff'=>$staff,
							'facilities'=>$facilities,
							'cleanliness'=>$cleanliness,
							'comfort'=>$comfort,
							'valueformoney'=>$valueformoney,
							'location'=>$location,
							'originalcomment'=>$originalcomment,
							'translation'=>$translation,
						);
			$insert	=	$db->insert('reports',$data);
			if($insert){
				header('location:add-report.php?msg=ras');
				exit;
			}else{
				header('location:add-report.php?msg=rna');
				exit;
			}
		}else{
			header('location:'.$_SERVER['PHP_SELF'].'?msg=dsd');
			exit;
		}
	
}
?>

<!doctype html>

<html lang="en-US" xmlns:fb="https://www.facebook.com/2008/fbml" xmlns:addthis="https://www.addthis.com/help/api-spec"
	prefix="og: http://ogp.me/ns#" class="no-js">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Review Report - Add Review</title>

	<?php include 'include/assets.php'; ?>
</head>

<body>
	<?php include 'include/header.php'; ?>



	<div id="addarea">
		<div class="container">
			<?php
		
		if(isset($_REQUEST['msg']) and $_REQUEST['msg']=="ras"){
			echo	'<div class="alert alert-success alert-dismissible fade show" role="alert"><i class="fa fa-thumbs-up"></i> Review added successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="rna"){
			echo	'<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="fa fa-exclamation-triangle"></i> Review not added <strong>Please try again!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
		}elseif(isset($_REQUEST['msg']) and $_REQUEST['msg']=="dsd"){
			echo	'<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="fa fa-exclamation-triangle"></i> Please delete a review and then try again <strong>We set limit for security reasons!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
		}
		?>

			<div class="card">
				<div class="card-header"><i class="fa fa-fw fa-plus-circle"></i> <strong>Add Review</strong> <a
						href="index.php" class="float-right btn btn-dark btn-sm"><i class="fa fa-fw fa-globe"></i>
						Browse Reviews</a></div>
				<div class="card-body">



					<div class="col-sm-6">

						<h5 class="card-title">Fields with <span class="text-danger">*</span> are mandatory!</h5>

						<form method="post" name="addreport">

							<div class="form-group">
								<label>Date <span class="text-danger">*</span></label>
								<input type="text" name="date" id="date" class="form-control" placeholder="Enter Date" required>
							</div>
							
							<div class="form-group">
								<label>Room Type <span class="text-danger">*</span></label>
								<select name="roomtype" id="roomtype" class="form-control" required>
									<option value="">Select Room Type</option>
									<option value="SSB">SSB</option>
									<option value="SPB">SPB</option>
									<option value="BDT">BDT</option>
									<option value="DBR">DBR</option>
									<option value="SDT">SDT</option>
									<option value="SWB">SWB</option>
									<option value="TR">TR</option>
								</select>
							</div>

							<div class="form-group">
								<label>Room Number <span class="text-danger">*</span></label>
								<select name="roomnumber" id="roomnumber" class="form-control" required>
									<option value="">Select Room Number</option>
								</select>
							</div>

							

							<div class="form-group">
								<label>Brand <span class="text-danger">*</span></label>
								<select name="brand" id="brand" class="form-control" required>
									<option value="">Select Brand</option>
									<option value="Khaosan Art Hotel">Khaosan Art Hotel</option>
									<option value="Casa Picasso">Casa Picasso</option>
									<option value="Happio">Happio</option>
								</select>
							</div>

							<div class="form-group">
								<label>Nationality <span class="text-danger">*</span></label>
								<input type="text" name="nationality" id="nationality" class="form-control" placeholder="Enter Nationality" required>
							</div>

							<div class="form-group">
								<label>Review Score <span class="text-danger">*</span></label>
								<input type="text" name="reviewscore" id="reviewscore" class="form-control" placeholder="Enter Review Score" required>
							</div>

							<div class="form-group">
								<label>Paste Score from Booking.com </label>
								<textarea name="copyscore" id="copyscore" class="form-control" placeholder="Paste Score"></textarea>
							</div>

							<div class="form-group">
								<label>Staff <span class="text-danger">*</span></label>
								<input type="text" name="staff" id="staff" class="form-control cpst_0" placeholder="Enter Staff" required>
							</div>
							
							<div class="form-group">
								<label>Facilities <span class="text-danger">*</span></label>
								<input type="text" name="facilities" id="facilities" class="form-control cpst_1" placeholder="Enter Facilities" required>
							</div>

							<div class="form-group">
								<label>Cleanliness <span class="text-danger">*</span></label>
								<input type="text" name="cleanliness" id="cleanliness" class="form-control cpst_2" placeholder="Enter Cleanliness" required>
							</div>

							<div class="form-group">
								<label>Comfort <span class="text-danger">*</span></label>
								<input type="text" name="comfort" id="comfort" class="form-control cpst_3" placeholder="Enter Comfort" required>
							</div>

							<div class="form-group">
								<label>Value for Money <span class="text-danger">*</span></label>
								<input type="text" name="valueformoney" id="valueformoney" class="form-control cpst_4" placeholder="Enter Value for Money" required>
							</div>

							<div class="form-group">
								<label>Location <span class="text-danger">*</span></label>
								<input type="text" name="location" id="location" class="form-control cpst_5" placeholder="Enter Location" required>
							</div>

							<div class="form-group">
								<label>Original Comment <span class="text-danger">*</span></label>
								<textarea name="originalcomment" id="originalcomment" class="form-control" placeholder="Enter Original Comment" required></textarea>
							</div>

							<div class="form-group">
								<label>Translation </label>
								<textarea name="translation" id="translation" class="form-control" placeholder="Enter Translation"></textarea>
							</div>

							

							<div class="form-group">

								<button type="submit" name="submit" value="submit" id="submit"
									class="btn btn-primary"><i class="fa fa-fw fa-plus-circle"></i> Add Review</button>

							</div>

						</form>

					</div>

				</div>

			</div>

		</div>
	</div>






	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
		integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous">
	</script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
		integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous">
	</script>

	<script>
		document.forms['addreport'].reset();

		$(document).ready(function(){
			// Paste script
			$('#copyscore').bind('paste', function(e) {
				setTimeout(function() {
					var lines = [];
					$.each($('#copyscore').val().split(/\n/), function(i, line){
					line = line.replace(/[^\d.-]/g, ''); 
						if(line){
							lines.push(line);
						} else {
							lines.push("");
						}
					});
					console.log(lines);	

					for(var i = 0; i <= 5; i++){
						$('.cpst_'+i).val(lines[i]);
					}


				}, 100);
			});

			var ssb = ['111','112','113','114','115','116','117','118','119','120','121'];
			var spb = ['218','219','220','318','319','320','418','420'];
			var bdt = ['211-D','212-D','213-D','214-D','215-D','216-D','217-D','216-T','217-T','311-D','312-D','313-D','314-T','315-D','316-T','317-T','411-D','412-D','413-D','414-D','415-D','416-D','417-D'];
			var dbr = ['221','222','223','224','225','321','322','323','324','325','421','422','423','424','425'];
			var sdt = ['101-D','102-D','103-D','203-T','204-D','205-D','206-D','207-D','208-D','209-D','303-D','304-T','305-T','306-D','307-D','308-D','309-D','403-D','404-T','405-T','406-D','407-D','408-D','409-D','503-T','504-T','505-T','506-D','507-D','508-D','509-D'];
			var swb = ['201-D','202-D','301-D','302-D','401-T','402-D','501-D','502-D','515-D','516-D','517-D','518-D','519-D'];
			var tr = ['511','512','513','514','518/Balcony'];

			$('#roomtype').change(function(){
				var v = $(this).val();
				var arr = [];
				var fopt = '<option value="">Select Room Type</option>';
				if(v != ''){
					if(v === 'SSB'){
						arr = ssb;
					}
					else if(v === 'SPB'){
						arr = spb;
					}
					else if(v === 'BDT'){
						arr = bdt;
					}
					else if(v === 'DBR'){
						arr = dbr;
					}
					else if(v === 'SDT'){
						arr = sdt;
					}
					else if(v === 'SWB'){
						arr = swb;
					}
					else if(v === 'TR'){
						arr = tr;
					}
					$('#roomnumber').empty();
					for(var i = 0; i <= arr.length-1; i++){
						fopt += '<option value="'+arr[i]+'">'+arr[i]+'</option>';
					}
					$(fopt).appendTo('#roomnumber');
				}
				else{
					$('#roomnumber').empty();
					var fopt = '<option value="">Select Room Type</option>';
					$(fopt).appendTo('#roomnumber');
				}
			})

		});
	</script>



</body>

</html>